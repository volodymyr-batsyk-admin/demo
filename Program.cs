﻿using System;
using System.IO;

namespace WalletIDisposable
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine(Wallet.Bonus);
            Wallet.Bonus += 100;

            var wallet1 = new Wallet(150);
            Console.WriteLine(wallet1.GetBalance());

            //MyFile file = null;
            //try
            //{
            //    file = new MyFile("C://file.txt");
            //}
            //finally
            //{
            //    file.Dispose();
            //}

            // The same
            using (MyFile file = new MyFile("C://file//file.txt"))
            {
                file.AddTextToFile("Hello");
            }
        }


        public class Wallet
        {
            public static int Bonus = 100;
            public static int Balance;

            public  Wallet(int bal)
            {
                Balance = bal;
            }

            public int GetBalance()
            {
                return Balance + Bonus;
            }

            public static int GetPureBalance()
            {
                return Balance;
            }
        }

        class MyFile : IDisposable
        {
            private FileStream MyFileStream { get; set; }
            private StreamWriter MyStream { get; set; }

            public MyFile(string filePath)
            {
                MyFileStream = File.Create(filePath);
                // Open stream for working
                MyStream = new StreamWriter(MyFileStream);
            }

            public void AddTextToFile(string text) => MyStream?.Write(text);

            // Fre memory
            public void Dispose()
            {
                MyStream?.Dispose();
                MyFileStream?.Dispose();
            }
        }

    }
}
